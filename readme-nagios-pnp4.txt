// README SOBRE LA INSTALACIÓN PLUGINS PNP4 NAGIOS

//primer paso ingresar al servidor de nagios y aplicar los siguientes comandos

yum install -y rrdtool perl-Time-HiRes rrdtool-perl php-gd
wget https://downloads.sourceforge.net/project/pnp4nagios/PNP-0.6/pnp4nagios-0.6.26.tar.gz
tar -xzvf pnp4nagios-0.6.26.tar.gz
cd pnp4nagios-0.6.26/
./configure
make all
make fullinstall
chkconfig --add npcd && chkconfig --level 35 npcd on
systemctl restart httpd
http://192.168.159.142/pnp4nagios
mv /usr/local/pnp4nagios/share/install.php /usr/local/pnp4nagios/share/install.php.BKP
cd /usr/local/nagios/etc/
vim nagios.cfg
// luego agregar al último del archivo

process_performance_data=1
# service performance data
service_perfdata_file=/usr/local/pnp4nagios/var/service-perfdata
service_perfdata_file_template=DATATYPE::SERVICEPERFDATA\tTIMET::$TIMET$\tHOSTNAME::$HOSTNAME$\tSERVICEDESC::$SERVICEDESC$\tSERVICEPERFDATA::$SERVICEPERFDATA$\tSERVICECHECKCOMMAND::$SERVICECHECKCOMMAND$\tHOSTSTATE::$HOSTSTATE$\tHOSTSTATETYPE::$HOSTSTATETYPE$\tSERVICESTATE::$SERVICESTATE$\tSERVICESTATETYPE::$SERVICESTATETYPE$
service_perfdata_file_mode=a
service_perfdata_file_processing_interval=15
service_perfdata_file_processing_command=process-service-perfdata-file

# host performance data
host_perfdata_file=/usr/local/pnp4nagios/var/host-perfdata
host_perfdata_file_template=DATATYPE::HOSTPERFDATA\tTIMET::$TIMET$\tHOSTNAME::$HOSTNAME$\tHOSTPERFDATA::$HOSTPERFDATA$\tHOSTCHECKCOMMAND::$HOSTCHECKCOMMAND$\tHOSTSTATE::$HOSTSTATE$\tHOSTSTATETYPE::$HOSTSTATETYPE$
host_perfdata_file_mode=a
host_perfdata_file_processing_interval=15
host_perfdata_file_processing_command=process-host-perfdata-file
//===========================================================================
cd objects
vim commands.cfg
// agregar esto en la última línea
define command{
       command_name    process-service-perfdata-file
       command_line    /bin/mv /usr/local/pnp4nagios/var/service-perfdata /usr/local/pnp4nagios/var/spool/service-perfdata.$TIMET$
}

define command{
       command_name    process-host-perfdata-file
       command_line    /bin/mv /usr/local/pnp4nagios/var/host-perfdata /usr/local/pnp4nagios/var/spool/host-perfdata.$TIMET$
}
//=============================================================================

vim templates.cfg
//añadir a la última línea

define host {
    name        host-pnp
    action_url  /pnp4nagios/index.php/graph?host=$HOSTNAME$&srv=_HOST_' class='tips' rel='/pnp4nagios/index.php/popup?host=$HOSTNAME$&srv=_HOST_
    register    0
}

define service {
    name        srv-pnp
    action_url  /pnp4nagios/index.php/graph?host=$HOSTNAME$&srv=$SERVICEDESC$' class='tips' rel='/pnp4nagios/index.php/popup?host=$HOSTNAME$&srv=$SERVICEDESC$  
    register    0
}

//=============================================================================
// ir donde tienes la carpeta de instalacion de pnp4nagios
cp contrib/ssi/status-header.ssi /usr/local/nagios/share/ssi/
systemctl restart npcd
nagioscheck
nagiosreload
cd /usr/local/nagios/etc/objects/
vim linux.cfg
// colocar  en el define host en la variable ejemplo linux-server,host-pnp
// colocar  en el define services en la variable ejemplo use generic-service,srv-pnp
nagioscheck
nagiosreload
