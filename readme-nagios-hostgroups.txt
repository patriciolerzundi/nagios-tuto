

cd /usr/local/nagios/etc/objects/
vim linux.cfg

// agregar en el archivo vim linux.cfg
############## Denicion de hostgroups  ######################
define hostgroup {
        hostgroup_name  Servidores_Linux
        alias           Servidores_Linux
        members         cliente_linux // se pueden agregar mas clientes
//===========================================================
nagioscheck
nagiosreload

// volver a editar el archivo vim linux.cfg
############## Definicion de servicios #######################
define service{
        use                     generic-service,srv-pnp
        #host_name              cliente_linux
        hostgroup_name          Servidores_Redhat
        service_description     Hard Disk
        check_command           check_nrpe!check_hda1

}
###############################################################

nagioscheck
nagiosreload

//esto es para crear un service group
vi linux.cfg

// ir a la última línea

############## Definicion Grupos De Servicios ##############
define servicegroup{
        servicegroup_name       All_Disk
        alias                   All_Disk
}
############# FIN SERVICE GROUPS ###########################



############# SE EDITA CUALQUIER SERVICIO ##############
define service{
        use                     generic-service,srv-pnp
        #host_name              cliente_linux
        hostgroup_name          Servidores_Redhat
        service_description     Hard Disk
        servicegroups           All_Disk
        check_command           check_nrpe!check_hda1

}
############ FIN ######################################

nagioscheck

