// readme para asingar tu servidor local al template en linux

cd /usr/local/nagios/etc/objects
vim localhost.cfg // solo revisar configuracion

cd /root/ // En donde pueden estar los archivos nrpe
cd /nrpe-3.2.1
./configure
make all
make install
make install-config
make install-init
systemctl enable nrpe
systemctl start nrpe
systemctl status nrpe

firewall-cmd --list-all //revisar que esté el nrpe

cd /usr/local/nagios/libexec/
./check_nrpe -H 127.0.0.1
cd ..
cd /etc/objects
vim linux.cfg

// colocar en define host
define host{
        use             linux-server,host-pnp
        host_name       nagios_server
        alias           Linux Centos
        address         127.0.0.1
}

define hostgroup {
        hostgroup_name  Servidores_Linux
        alias           Servidores_Linux
        members         cliente_linux,nagios_server // colocar en el member
}
//========================================
nagioscheck
nagiosreload
df -h


vim /usr/local/nagios/etc/nrpe.cfg

// luego editar
command[check_hda1]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /dev/mapper/centos_nagios-root
//============================================
systemctl restart nrpe

vim /usr/local/nagios/etc/nrpe.cfg

// colocar el siguiente comando
command[check_swap]=/usr/local/nagios/libexec/check_swap -w 20% -c 10%
command[check_uptime]=/usr/local/nagios/libexec/check_uptime

nagioscheck
systemctl restart nrpe
cd ..
vim nagios.cfg
// comentar esto en el archivos
#cfg_file=/usr/local/nagios/etc/objects/localhost.cfg
nagioscheck
nagiosreload
