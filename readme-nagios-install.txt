# documento para la instalación nagios core

cd /etc/selinux/config # no es recomendable para produccion
SELINUX=disabled

firewall-cmd --permanent --add-port=80/tcp
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --reload
firewall-cmd --list-all
systemctl reboot

yum install -y gettext wget net-snmp-utils openssl-devel glibc-common unzip perl epel-release gcc php gd automake autoconf httpd make glibc gd-devel net-snmp git

yum install -y perl-Net-SNMP

useradd nagios
usermod -a -G nagios apache
id nagios
id apache

wget https://github.com/NagiosEnterprises/nagioscore/releases/download/nagios-4.4.2/nagios-4.4.2.tar.gz
tar  -xzvf nagios-4.4.2.tar.gz
cd /nagios-4.4.2
./configure # adentro de la instalacion
make all
make install
make install-init
make install-commandmode
make install-config
make install-webconf
systemctl enable nagios
systemctl enable httpd
systemctl is-enabled nagios
systemctl is-enabled httpd

// recordar si hacen pro primera vez un usuario se agrega -c en caso
// contrario se quita -c sino van a sobreescribir a otros usuarios

htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin


//=============================================================================

nano /usr/local/nagios/etc/cgi.cfg
(in cgi.cfg file, look for this section):

# GLOBAL HOST/SERVICE VIEW ACCESS
# These two options are comma-delimited lists of all usernames that
# can view information for all hosts and services that are being
# monitored.  By default, users can only view information
# for hosts or services that they are contacts for (unless you
# you choose to not use authorization). You may use an asterisk (*)
# to authorize any user who has authenticated to the web server.
(at the end of the line, add the user "new_user"):

authorized_for_all_services=nagiosadmin,new_user
authorized_for_all_hosts=nagiosadmin,new_user



//============================================================================

systemctl start nagios
systemctl start httpd
systemctl status nagios
systemctl status httpd

############ PLUGINS NAGIOS ############

wget https://github.com/nagios-plugins/nagios-plugins/releases/download/release-2.2.1/nagios-plugins-2.2.1.tar.gz
tar -xzvf nagios-plugins-2.2.1.tar.gz
cd nagios-plugins
./configure
make install
systemctl restart nagios
cd /usr/local/nagios/libexec #ruta en donde están los plugins


############### NRPE #######################

wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.2.1/nrpe-3.2.1.tar.gz // permite conectar los servidores clientes
tar -xvzf nrpe-3.2.1.tar.gz
cd nrpe-3.2.1.tar.gz
./configure
make check_nrpe
make install-plugin
cd /usr/local/nagios/etc/objects/
vi commands.cfg
// agregar a la ultima linea de commands.cfg

define command {
    command_name   check_nrpe
    command_line   $USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$

}


cd ..
cat resource.cfg

############ INSTALACION AGENTE LINUX #############

vim /etc/selinux/config
// configurar SELINUX=disabled
firewall-cmd --permanent --add-port=5666/tcp
firewall-cmd --reload
systemctl reboot

yum install -y gcc glibc glibc-common openssl openssl-devel perl wget
useradd nagios
wget https://github.com/nagios-plugins/nagios-plugins/releases/download/release-2.2.1/nagios-plugins-2.2.1.tar.gz
tar xzvf nagios-plugins-2.2.1.tar.gz
cd nagios-plugins
./configure
make install
wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.2.1/nrpe-3.2.1.tar.gz
tar -xvzf nrpe-3.2.1.tar.gz
cd nrpe
./configure
make all
make install
make install-config
make install-init
cd /usr/local/nagios/etc/
vim nrpe.cfg //configurar allowed_host colocando una , y luego la ip
systemctl enable nrpe
systemctl start nrpe
// en el lado del servidor colocar lo siguiente para verificar la version del nrpe
cd /usr/local/nagios/libexec
./check_nrpe -H 192.168.159.143 // ip del cliente y debe volver la version instalada


################ CREACION TEMPLATE CLIENTE AGENTE ########################
// ingresar al server y aplicar los siguientes documentos
cd /usr/local/nagios/etc/objects
vim linux.cfg
// colocar en el documento
############### Definicion de servidores #####################
define host{
        use             linux-server
        host_name       cliente_linux
        alias           Linux centos
        check_interval  1
        address         192.168.159.143
}
############## Definicion de servicios #######################
define service{
        use                     generic-service
        host_name               cliente_linux
        service_description     Hard Disk
        check_command           check_nrpe!check_hda1

}
define service{
        use                     generic-service
        host_name               cliente_linux
        service_description     Uptime
        check_command           check_nrpe!check_uptime
}
define service{
        use                     generic-service
        host_name               cliente_linux
        service_description     Swap
        check_command           check_nrpe!check_swap
}

define service{
        use                     generic-service
        host_name               cliente_linux
        service_description     Ping
        check_command           check_ping!500.0,20%!800.0,60%
}
define service{
        use                     generic-service
        host_name               cliente_linux
        service_description     Current Users
        check_command           check_nrpe!check_users
}
################################################################
cd /usr/local/nagios // en el server
vim nagios.cfg
// colocar el template en cfg_file=
cfg_file=/usr/local/nagios/etc/objects/linux.cfg
// revisa si hay error en el template
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
systemctl restart nagios

// metodo abreviado para reiniciar nagios y checkear archivos cfg servidor
cd
pwd // para verificar que están en root
vim .bashrc
// colocarlas al ultimo del documento .bashrc
alias nagioscheck='/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg'
alias nagiosreload='systemctl restart nagios'
source .bashrc

//configurar refresh servidor nagios
cd /usr/local/nagios/etc/
vi cgi.cfg
refresh_rate=10
// luego ejecutar los comandos
nagioscheck
nagiosreload

// configurtacion del linux cliente
vim /usr/local/nagios/etc/nrpe.cfg
// buscar en los parametros command
command[check_hda1]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /dev/mapper/centos_cl-root
command[check_swap]=/usr/local/nagios/libexec/check_swap -w 20% -c 10%
command[check_uptime]=/usr/local/nagios/libexec/check_uptime

// reiniciar del lado del cliente para los cambios
 systemctl restart nrpe

// en caso reducir lineas de codigo sobre intervalos:
cambiar check_interval a 1 host,services
